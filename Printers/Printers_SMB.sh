#!/bin/sh

Server="5057CPS01.curric.lilydale-west-ps.wan"

RemovePrinters=("Photocopy Room B&W" "Photocopy Room Colour" "Junior Building Copier" "AW-Colour-C5240" "AW-GrayScale-6275" "BW-Colour-C280" "AW-Colour-C5540i")

DriverPKGs=("Canon_PS_Installer.pkg" "bizhub_C360_109.pkg")
AddPrinters=("AW-Colour-C5540i" "AW-GrayScale-6275" "BW-Colour-C280")

for dp in "${DriverPKGs[@]}"; do
	if [ ! -f /tmp/$dp ]; then
		echo "$dp missing downloading..."
		curl -x $Proxy -o /tmp/$dp $Repository/Printers/$dp
	fi
	sudo installer -pkg /tmp/$dp -target /
	sleep 3
done
sleep 2

for rp in "${RemovePrinters[@]}"; do
	echo "Remove printer $rp"
	lpadmin -x $rp
done
sleep 2

for ap in "${AddPrinters[@]}"; do
	echo "Add Printer $ap"
	curl -x $Proxy -o /tmp/$ap.ppd $Repository/Printers/ppd/$ap.ppd
	lpadmin -p $ap -E -v smb://$Server/$ap -P /tmp/$ap.ppd -o printer-is-shared=false -o auth-info-required=negotiate
done
sleep 2