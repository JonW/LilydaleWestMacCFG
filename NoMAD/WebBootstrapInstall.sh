#!/bin/sh
echo "Start NoMAD Web Bootstrap Installer"

NeedFiles=("NoMAD.pkg" "NoMAD-LaunchAgent.pkg" "NoMad-VictoriaRoadPS.mobileconfig" "NoMAD-VictoriaRoadPS-Shares.mobileconfig")

NoMADTMP=/tmp/NoMAD

mkdir $NoMADTMP

for nf in "${NeedFiles[@]}"; do
	echo "Download File $nf"
	curl -x $Proxy -o $NoMADTMP/$nf $Repository/NoMAD/$nf
done

#remove old kerberos keeper
launchctl unload -w ~/Library/LaunchAgents/com.kerberoskeeper.plist
sudo rm ~/Library/LaunchAgents/com.kerberoskeeper.plist

#configure and install NoMAD
sudo /usr/bin/profiles -I -F $NoMADTMP/NoMad-VictoriaRoadPS.mobileconfig
sudo /usr/bin/profiles -I -F $NoMADTMP/NoMAD-VictoriaRoadPS-Shares.mobileconfig
sudo installer -pkg $NoMADTMP/NoMAD.pkg -target /
sudo installer -pkg $NoMADTMP/NoMAD-LaunchAgent.pkg -target /
