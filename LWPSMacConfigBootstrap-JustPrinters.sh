#!/bin/sh

pause(){
 read -n1 -rsp $'Press any key to continue or Ctrl+C to exit...\n'
}

#this should not be run as root
if [[ $EUID -eq 0 ]]; then
   echo "This script must NOT be run as root"
   exit 1
fi

Repository="https://gitlab.com/JonW/LilydaleWestMacCFG/raw/master"
Proxy="http://10.152.62.19:8080"

echo "Start Mac Configuration Bootstrap"

echo "Install Printers"
curl -x $Proxy -o /tmp/Printers_SMB.sh $Repository/Printers/Printers_SMB.sh
source /tmp/Printers_SMB.sh