LWPS Mac Config
==============
This utility is used to configure the occasional school mac that a staff member may use.

## Requirements
This utility requires network access at LWPS prior to running

## KerberosKeeper
kerberos Keeper is a small app that renews kerberos tickets for mac's so they "Just Work" in AD networks.

## NetworkMaps
Maps shared drives for staff and attaches them to the dock for easy access.

## WAUOffice2016
Configures Office Automatic Updates to check and install updates automaticly. Also has the
ability to be pointed to a local office cache of files. See [link](https://gitlab.com/JonW/MAUCacheAdmin) for more info.

## Printers 
You generate the PPD's on a mac with the same driver that you are going to use.
Set the defaults in the cups web interface http://127.0.0.1:631 (paper size, grayscale, finisher's, etc)
then download the PPD by appending .ppd to the printer name in the cups web interface.

## Usage
bash .\LWPSMacConfigBootstrap.sh
