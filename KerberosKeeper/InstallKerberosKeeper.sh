#!/bin/sh
echo "Running as"
whoami
echo "ID is"
id -u

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "Our script dir is $DIR"

sudo mkdir "/Library/Application support/kerberoskeeper"
sudo cp "$DIR/KerberosKeeper.py" "/Library/Application support/kerberoskeeper/"
sudo cp "$DIR/KerberosKeeper-Logo.gif" "/Library/Application support/kerberoskeeper/"
sudo cp "$DIR/RemoveKerberosKeeper.sh" "/Library/Application support/kerberoskeeper/"

sudo mkdir ~/Library/LaunchAgents
sudo cp "$DIR/com.kerberoskeeper.plist" ~/Library/LaunchAgents/

launchctl load -w ~/Library/LaunchAgents/com.kerberoskeeper.plist

launchctl list | grep kerberoskeeper