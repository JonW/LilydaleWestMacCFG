#Created by Jonathan Ward for Lilydale Heights college
#Version 20160912

import time
import subprocess
import json
import datetime
import os
import plistlib
import re
import base64
from Tkinter import * #http://effbot.org/tkinterbook/label.htm

SettingsFile = "com.kerberoskeeper.plist" #plist file used to store Kerberos Keeper's settings
#Static Settings
KerberosRelm = "CURRIC.LILYDALE-WEST-PS.WAN" #the Kerberos relm to keep the users ticket for.
TicketRenewPeriod = 600 #renew tickets when they have this long left on their life.
RageCloseDuration= 3600 #wait this many seconds before asking again if the user rage closes the password popup.

DialogBoxTitle = "Lilydale West Primary School"
DialogBoxImage = "KerberosKeeper-Logo.gif"

#Start of app
def Print_Log(string):
    print(str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))+": "+str(string))

class Kerberos():
    def __init__(self,errmsg=False):
        self.settingspath = os.path.expanduser('~/Library/Preferences/'+SettingsFile)
        self.Settings_Load()
        
        #first we test for KDC
        if(self.kfind()):
            if(self.settings["Principal"]):
                self.Password = False
            else:
                #Test for rage close of popup.
                if(self.settings["RageCloseTimeout"] > self.Time_Now()):
                    Print_Log("User Rage detected will start again at "+str(self.settings["RageCloseTimeout"]))
                    exit(1)
                else:
                    self.settings["RageCloseTimeout"] = False
                    self.Get_Credential(errmsg)
                    self.kdestroy()
            if(self.klist()):
                Print_Log("Your Tickets are valid we dont need to do anything.")
                self.Settings_Save()
            else:
                if(self.kinit()):
                    self.Settings_Save()
                    Print_Log("kinit sucsess")
                else:
                    Print_Log("Useername or Password incorrect")
                    self.settings["Principal"] = False
                    self.Settings_Save()
                    Kerberos("Username or password incorrect.")
        else:
            Print_Log("No avalibe KDC's")
            exit(0)
        
    def Settings_Load(self):
        try:
            self.settings = plistlib.readPlist(self.settingspath)
        except:
            Print_Log("Generate Local plist")
            Username = self.getPrincipalKeychain()
            self.settings = dict(Principal=Username,RageCloseTimeout=False)
            self.Settings_Save()

    def Settings_Save(self):
        try:
            plistlib.writePlist(self.settings, self.settingspath)
        except Exception as e:
            Print_Log("plist save failed with :"+str(e))

    def Time_Now(self):
        "return time format used in this app and in kerberos"
        return int(datetime.datetime.now().strftime('%Y%m%d%H%M%S'))

    def Get_Credential(self,errmsg=False):
        "Asks user for username and password, if you need a error message send it as a string"
        #http://stackoverflow.com/questions/15724658/simplest-method-of-asking-user-for-password-using-graphical-dialog-in-python
        #Modified from this source.
        root = Tk()
        root.title(DialogBoxTitle)

        global PASSWORD,USERNAME
        PASSWORD = False
        USERNAME = False
        def onpwdentry(evt):
            global PASSWORD,USERNAME
            USERNAME = usrbox.get()
            PASSWORD = pwdbox.get()
            root.destroy()
        def onokclick():     
            global PASSWORD,USERNAME
            USERNAME = usrbox.get()
            PASSWORD = pwdbox.get()
            root.destroy()

        usrbox = Entry(root)
        pwdbox = Entry(root,show = '*')

        if(DialogBoxImage):
            FullImagePath = os.path.dirname(os.path.abspath(__file__))+"/"+DialogBoxImage
            photo = PhotoImage(file = FullImagePath)
            label = Label(root,image = photo)
            label.image = photo # keep a reference!
            label.grid(padx=5, pady=5, column=0, row=0, rowspan=5)

        Label(root, text = "Internet access expired.\n Enter your school username and password to continue.", font=("Helvetica", 16)).grid(column=1,row=0,columnspan=2)
        
        if(errmsg):
            Label(root, text = errmsg, fg="red").grid(column=1,row=1,columnspan=2)

        Label(root, text = 'UserName', font=("Helvetica", 16)).grid(row=2,column=1)
        usrbox.grid(row=2,column=2)

        Label(root, text = 'Password', font=("Helvetica", 16)).grid(row=3,column=1)
        pwdbox.grid(row=3,column=2)
        
        pwdbox.bind('<Return>', onpwdentry)
        Button(root, command=onokclick, text = 'OK').grid(column=2)

        root.mainloop()
        if((PASSWORD) and (USERNAME)):
            if(re.match('^[a-zA-Z0-9]{0,14}$',USERNAME)):
                self.Password = PASSWORD
                self.settings["Principal"] = USERNAME
                Print_Log("Username and Password Accepted")
                return True
            else:
                self.Get_Credential("You must supply a valid UserName and Password.")
        if((PASSWORD is False) or (USERNAME is False)):
            Print_Log("Rage quit detected set rage quit timeout")
            self.settings["RageCloseTimeout"] = int(self.Time_Now() + int(RageCloseDuration))
            self.Settings_Save()
            exit(10)
            return False
        else:
            Print_Log("Password Bad")
            self.Get_Credential("You must supply a UserName and Password.")
            return False
    
    def klist(self):
        Print_Log("Check for local kerberos tickets")
        klist = subprocess.Popen(["klist", "--json"],stdout=subprocess.PIPE)
        out , error = klist.communicate()
        klist.stdout.close()
        if (klist.returncode == 0):
            jsonout = json.loads(out)
            #Check for valid tickets.
            if("principal" in jsonout):
                Print_Log(jsonout["principal"])
                for ticket in jsonout["tickets"]:
                    if("krbtgt/"+KerberosRelm in ticket["Principal"]):
                        if('Expired' in ticket["Expires"]):
                            #check for expired '>>>>Expires<<<<'
                            TicketExpires = 0
                            TicketRenew = 0
                        else:
                            if(' ' in ticket["Expires"]):
                                #need to convert time for 10.9 'Nov 30 22:48:45 2016'
                                ticket["Expires"] = datetime.datetime.strptime(ticket["Expires"], "%b %d %H:%M:%S %Y").strftime('%Y%m%d%H%M%S')
                            TicketExpires = int(ticket["Expires"]) - self.Time_Now()
                            TicketRenew = int(TicketExpires) - int(TicketRenewPeriod)
                        Print_Log(ticket["Principal"]+". Ticket expires in: "+str(TicketExpires)+" seconds. Renew begins in: "+str(TicketRenew)+" seconds.")
                        if(TicketExpires <= 0):
                            Print_Log(ticket["Principal"]+" has expired")
                            return False
                        elif(TicketRenew <= 0):
                            Print_Log(ticket["Principal"]+" is due to expire soon")
                            return False
                        else:
                            Print_Log(ticket["Principal"]+" is valid")
                            return True
                Print_Log("No krbtgt/"+KerberosRelm+" ticket found")
                return False
            else:
                Print_Log("No tickets.")
                return False
        else:
            Print_Log("Error reading tickets.")
            return False

    def kinit(self):
        FQP = self.settings["Principal"]+"@"+KerberosRelm
        Print_Log("Your Fully Qualified Principal is:"+FQP)
        kinit = subprocess.Popen(["kinit", "--keychain", "-V", FQP],stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        if(self.Password):
            Print_Log("writing Password to stdin")
            kinit.stdin.write(self.Password+'\n')
            kinit.stdin.flush()
        kinitoutput,kiniterror = kinit.communicate()
        
        kinit.stdout.close()
        kinit.stdin.close()
        #Print_Log(kinitoutput)
        #Print_Log(kiniterror)
        #Print_Log(kinit.returncode)

        if(kinit.returncode == 0):
            return True
        elif('unable to reach any KDC in realm' in kiniterror):
            Print_Log("unable to reach any KDC. Exiting.")
            exit(11)
        else:
            return False

    def kdestroy(self):
        FQP = self.settings["Principal"]+"@"+KerberosRelm
        Print_Log("Destroy Kerberos Keys for :"+FQP)
        subprocess.call(["kdestroy","--principal="+FQP])
        return True
    
    def kfind(self):
        Print_Log("Looking for KDC's in: "+KerberosRelm)
        #the Time=2 and tries=1 will take a max of 2 seconds to run. but it will loop 8 times
        KfindCount = 1
        while (KfindCount <= 8):
            Print_Log("Looking for KDC attempt: "+str(KfindCount))
            kfind = subprocess.Popen(["dig","srv","_kerberos._tcp."+KerberosRelm, "+short","+time=2","+tries=1"],stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        
            kfindoutput,kfinderror = kfind.communicate()
        
            kfind.stdout.close()
            kfind.stdin.close()
            Print_Log(kfindoutput)
            #Print_Log(kfinderror)
            #Print_Log(kfind.returncode)
            if('0 100 88' in kfindoutput):
                Print_Log("there is a KDC avalible")
                return True
            KfindCount = KfindCount + 1
        return False

    def getPrincipalKeychain(self):
        Print_Log("Looking in keychain for username")
        securityData = subprocess.Popen(["security", "-v", "find-generic-password", "-l",KerberosRelm],stdout=subprocess.PIPE, stderr=subprocess.PIPE)       
        securityoutput,securityerror = securityData.communicate()     
        securityData.stdout.close()
        #print(str(securityoutput))
        if(securityData.returncode == 0):
            p = re.compile(ur'\"acct\"<blob>=\"([a-zA-Z0-9]{2,20})\"$', re.MULTILINE)
            Username = re.findall(p, securityoutput)

            if(Username and Username[0]):
                Print_Log("Found Username in keychain")
                return Username[0]
            else:
                Print_Log("Username not in keychain")
                return False
        else:
            Print_Log("Username not in keychain")
            return False

#sleep on startup so we catch dns changes.
#time.sleep(4)

Kerb = Kerberos()