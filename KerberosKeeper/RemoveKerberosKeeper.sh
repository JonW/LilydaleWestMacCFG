#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "Our script dir is $DIR"

sudo rm -rf "/Library/Application support/kerberoskeeper"

launchctl unload -w ~/Library/LaunchAgents/com.kerberoskeeper.plist

sudo rm ~/Library/LaunchAgents/com.kerberoskeeper.plist

launchctl list | grep kerberoskeeper