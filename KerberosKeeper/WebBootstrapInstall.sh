#!/bin/sh
echo "Start KerberosKeeper Web Bootstrap Installer"

NeedFiles=("com.kerberoskeeper.plist" "InstallKerberosKeeper.sh" "RemoveKerberosKeeper.sh" "KerberosKeeper.py" "KerberosKeeper-Logo.gif")

mkdir /tmp/kerberoskeeper

for nf in "${NeedFiles[@]}"; do
	echo "Download File $nf"
	curl -x $Proxy -o /tmp/kerberoskeeper/$nf $Repository/KerberosKeeper/$nf
done

bash /tmp/kerberoskeeper/InstallKerberosKeeper.sh