#!/bin/sh
echo "Start Web Bootstrap Customizer"

NeedFiles=("WAUOffice2016.sh" "NetworkMaps.sh")

mkdir /tmp/Customize

for nf in "${NeedFiles[@]}"; do
	echo "Download File $nf"
	curl -x $Proxy -o /tmp/Customize/$nf $Repository/Customize/$nf
	echo "Running file $nf"
	bash /tmp/Customize/$nf
done