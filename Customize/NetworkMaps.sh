#!/bin/sh
Server="smb://5057cfs01.curric.lilydale-west-ps.wan/"

AddMaps=("users$" "shared$")

klist -t
if [ $? -ne 0 ]; then
	echo "You are missing your kerberos ticket."
    pause
fi

sleep 5
for am in "${AddMaps[@]}"; do
	echo "Adding Map $Server$am"
	osascript -e 'mount volume "'"$Server$am"'"'
	defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Volumes/'"$am"'</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
done

killall Dock