#!/bin/sh
#
#Set the UpdateCache Location. 
#
#defaults write com.microsoft.autoupdate2 UpdateCache -string 'http://maucache.curric.wonga-park-ps.wan/'

#
#Set the update method.
#
#Disable Automatic Check for updates.
#defaults write com.microsoft.autoupdate2 HowToCheck -string 'Manual'

#Automaticly Check for updates (Default)
#defaults write com.microsoft.autoupdate2 HowToCheck -string 'AutomaticCheck'

#Automaticly Download and install updates.
defaults write com.microsoft.autoupdate2 HowToCheck -string 'AutomaticDownload'

#
#Set the ring name
#
#Office Insider slow ring.
#defaults write com.microsoft.autoupdate2 ChannelName -string 'External'

#Office Insider fast ring.
#defaults write com.microsoft.autoupdate2 ChannelName -string 'InsiderFast'

#remove ChanelName to set client to production ring.
defaults remove com.microsoft.autoupdate2 ChannelName
